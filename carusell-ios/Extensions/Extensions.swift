//
//  Extensions.swift
//  carusell-ios
//
//  Created by Adrian Cimpean on 08.05.2021.
//

import Foundation
import UIKit
import PromiseKit

extension UIView {
    func addGradient(color: UIColor, startPoint: CGPoint?, endPoint: CGPoint?) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [color.cgColor, color.withAlphaComponent(0.7).cgColor, color.withAlphaComponent(0.3).cgColor]
        self.backgroundColor = .clear
        gradientLayer.frame = self.bounds
        if let startPoint = startPoint, let endPoint = endPoint {
            gradientLayer.startPoint = startPoint
            gradientLayer.endPoint = endPoint
        }
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
}

extension Promise {
    static var rejected: Promise { return Promise.init(error: NSError.rejectedError)}
}

extension NSError {
    static let rejectedError = NSError(domain: "default_errors", code: 0, userInfo: [NSLocalizedDescriptionKey: "Sorry, we couldn't handle this operation at the moment. Please try again later!"])
}
