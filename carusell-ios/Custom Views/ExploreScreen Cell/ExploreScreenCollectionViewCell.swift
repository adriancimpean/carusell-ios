//
//  ExploreScreenCollectionViewCell.swift
//  carusell-ios
//
//  Created by Adrian Cimpean on 19.04.2021.
//

import UIKit

protocol ExploreScreenCollectionViewCellDelegate: AnyObject {
    func didAskToReload()
}

class ExploreScreenCollectionViewCell: UICollectionViewCell {
    
    let announcementManager = AnnouncementManager()
    private var announcementId: Int = 0
    private var viewController: UIViewController?
    weak var delegate: ExploreScreenCollectionViewCellDelegate?
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var carImageView: UIImageView!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var kmLabel: UILabel!
    @IBOutlet weak var titleLable: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var addToFavoritesButton: UIButton!
    @IBAction func addToFavoritesButtonPressed(_ sender: Any) {
        for announcement in CurrentUser.shared.favoriteAnnouncementsIds {
            if announcement == announcementId {
                let snackbar = Snackbar(viewController: self.viewController!, title: "Announcement already marked as favorite", message: nil, severity: .warning)
                snackbar.show()
                return
            }
        }
        announcementManager.addAnnouncementToFavorites(announcementId: announcementId, userId: CurrentUser.shared.userId).done {
            [weak self] success in
            guard let self = self else { return }
            let snackbar = Snackbar(viewController: self.viewController!, title: success["message"].stringValue, message: nil, severity: .success)
            snackbar.show()
            CurrentUser.shared.favoriteAnnouncementsIds.append(self.announcementId)
            self.delegate?.didAskToReload()
        }.catch { error in print(error) }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.masksToBounds = true
        activityIndicator.startAnimating()
    }
    
    public func configure(title: String, price: String, year: String, kilometers: String, announcementId: Int, viewController: UIViewController) {
        setDefaultState()
        titleLable.text = title
        priceLabel.text = "\(price) eur"
        yearLabel.text = year
        kmLabel.text = "\(kilometers)km"
        self.announcementId = announcementId
        self.viewController = viewController

        announcementManager.getAnnouncementImages(announcementId: announcementId).done { [weak self] success in
            guard let self = self else { return }
            let photoUrl = success[0]["photo_url"].stringValue
            Utils.loadImage(imageUrl: photoUrl, imageView: self.carImageView)
            self.activityIndicator.stopAnimating()
        }.catch { error in print(error) }
        
        if(!CurrentUser.shared.favoriteAnnouncementsIds.isEmpty) {
            for announcement in CurrentUser.shared.favoriteAnnouncementsIds {
                if(announcement == self.announcementId) {
                    addToFavoritesButton.setImage(UIImage(systemName: "heart.fill"), for: .normal)
                }
            }
        }
    }
    
    func setDefaultState() {
        addToFavoritesButton.setImage(UIImage(systemName: "heart"), for: .normal)
    }
    
    static func nib() -> UINib {
        return UINib(nibName: "ExploreScreenCell", bundle: nil)
    }
}
