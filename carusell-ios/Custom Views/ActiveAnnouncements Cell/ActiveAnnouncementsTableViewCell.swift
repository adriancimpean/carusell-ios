//
//  ActiveAnnouncementsTableViewCell.swift
//  carusell-ios
//
//  Created by Adrian Cimpean on 30.04.2021.
//

import Foundation
import UIKit
import SwiftyJSON

public protocol ActiveAnnouncementsCellDelegate: AnyObject {
    func deleteButtonPressed(cell: UITableViewCell)
}

class ActiveAnnouncementsTableViewCell: UITableViewCell {
    weak var delegate: ActiveAnnouncementsCellDelegate?
    let announcementManager = AnnouncementManager()
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var carImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    private var announcementId: Int = 0
    private var viewController: UIViewController?
    private var vehicle: JSON?
    private var announcement: JSON?
    
    @IBAction func editButtonPressed(_ sender: Any) {
        guard let announcementFormVC = UIStoryboard(name: "ExploreScreen", bundle: nil).instantiateViewController(withIdentifier: "formScreen") as? AnnouncementFormViewController else { return }
        announcementFormVC.buttonActionType = .edit
        announcementFormVC.formTitle = "Edit Announcement"
        announcementFormVC.announcementId = announcementId
        announcementFormVC.announcementKm = vehicle?[0]["kilometers"].stringValue ?? ""
        announcementFormVC.announcementYear = vehicle?[0]["year"].stringValue ?? ""
        announcementFormVC.announcementEngineSize = vehicle?[0]["engine_size"].stringValue ?? ""
        announcementFormVC.announcementEnginePower = vehicle?[0]["power"].stringValue ?? ""
        announcementFormVC.announcementPrice = vehicle?[0]["price"].stringValue ?? ""
        announcementFormVC.announcementTitle = announcement?["title"].stringValue ?? ""
        announcementFormVC.announcementDescription = announcement?["description"].stringValue ?? ""
        viewController?.navigationController?.pushViewController(announcementFormVC, animated: true)
    }
    @IBAction func deleteButtonPressed(_ sender: Any) {
        announcementManager.deleteAnnouncement(id: announcementId).done { [weak self] success in
            guard let self = self else { return }
            let snackbar = Snackbar(viewController: self.viewController!, title: success["message"].stringValue, message: nil, severity: .success)
            snackbar.show()
            self.delegate?.deleteButtonPressed(cell: self)
        }.catch { error in print(error) }
    }
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    public func configure(viewController: UIViewController, announcementId: Int, title: String, announcement: JSON) {
        activityIndicator.startAnimating()
        titleLabel.text = title
        self.announcementId = announcementId
        self.viewController = viewController
        self.announcement = announcement
        announcementManager.getAnnouncementImages(announcementId: announcementId).done{ [weak self] success in
            guard let self = self else { return }
            let photoUrl = success[0]["photo_url"].stringValue
            Utils.loadImage(imageUrl: photoUrl, imageView: self.carImageView)
            self.activityIndicator.stopAnimating()
        }.catch { error in print(error) }
        
        announcementManager.getVehicle(id: announcement["vehicle_id"].stringValue).done { [weak self] success in
            self?.vehicle = success
        }.catch { error in print(error) }
    }
    
    static func nib() -> UINib {
        return UINib(nibName: "ActiveAnnouncementsScreenCell", bundle: nil)
    }
}
