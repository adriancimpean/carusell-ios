//
//  SnackBar.swift
//  carusell-ios
//
//  Created by Adrian Cimpean on 30.05.2021.
//

import Foundation
import UIKit

public enum SeverityType {
    case success
    case warning
    case error
    case other
}

public class Snackbar {
    
    // --MARK: CONSTANTS
    private let duration: Double = 2
    private let titleFont: UIFont = UIFont(name: "Poppins-Bold", size: 20)!
    
    private var title: String?
    private var message: String?
    private var severity: SeverityType
    private var viewController: UIViewController
    
    init(viewController: UIViewController, title: String?, message: String?, severity: SeverityType) {
        self.title = title
        self.message = message
        self.severity = severity
        self.viewController = viewController
    }
    
    public func show() {
        let snackbar = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        snackbar.view.clipsToBounds = true
        snackbar.view.layer.cornerRadius = 15
        snackbar.view.backgroundColor = getBackgroundColor(severity: severity)
        setFont(text: title, snackbar: snackbar)
        viewController.present(snackbar, animated: true)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + duration) {
            snackbar.dismiss(animated: true)
        }
    }
    
    private func getBackgroundColor(severity: SeverityType) -> UIColor {
        switch severity {
        case .error:
            return .systemRed
        case .warning:
            return .systemYellow
        case .success:
            return .systemGreen
        case .other:
            return .systemGray
        }
    }
    
    private func setFont(text: String?, snackbar: UIAlertController) {
        guard let text = text else { return }
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttributes([NSAttributedString.Key.font : titleFont], range: NSMakeRange(0, text.utf8.count))
        snackbar.setValue(attributedString, forKey: "attributedTitle")
    }
}
