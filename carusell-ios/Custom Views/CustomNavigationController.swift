//
//  CustomNavigationController.swift
//  carusell-ios
//
//  Created by Adrian Cimpean on 21.04.2021.
//

import UIKit

class CustomNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationBar.shadowImage = UIImage()
        navigationBar.tintColor = .white
    }
}
