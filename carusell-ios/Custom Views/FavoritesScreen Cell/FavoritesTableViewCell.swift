//
//  FavoritesTableViewCell.swift
//  carusell-ios
//
//  Created by Adrian Cimpean on 20.05.2021.
//

import UIKit

public protocol FavoritesAnnoucementsCellDelegate: AnyObject {
    func removeFromFavoritesButtonPressed(cell: UITableViewCell)
}

class FavoritesTableViewCell: UITableViewCell {

    weak var delegate: FavoritesAnnoucementsCellDelegate?
    private let announcementManager = AnnouncementManager()
    private var favoriteId: Int = 0
    private var announcementId: Int = 0
    private var viewController: UIViewController?
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var carImageView: UIImageView!
    @IBOutlet weak var announcementTitleLabel: UILabel!
    @IBAction func removeFromFavorites(_ sender: Any) {
        announcementManager.removeFromFavorites(favoriteAnnouncementId: favoriteId).done { [weak self] success in
            guard let self = self else { return }
            let snackbar = Snackbar(viewController: self.viewController ?? UIViewController(), title: success["message"].stringValue, message: nil, severity: .success)
            snackbar.show()
            self.delegate?.removeFromFavoritesButtonPressed(cell: self)
            if let index = CurrentUser.shared.favoriteAnnouncementsIds.firstIndex(of: self.announcementId) {
                CurrentUser.shared.favoriteAnnouncementsIds.remove(at: index)
            }
        }.catch{ error in print(error) }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        activityIndicator.startAnimating()
    }
    
    public func configure(title: String, favoriteId: Int, announcementId: Int, viewController: UIViewController) {
        announcementTitleLabel.text = title
        self.favoriteId = favoriteId
        self.announcementId = announcementId
        self.viewController = viewController
        announcementManager.getAnnouncementImages(announcementId: announcementId).done { [weak self] success in
            guard let self = self else { return }
            let photoUrl = success[0]["photo_url"].stringValue
            Utils.loadImage(imageUrl: photoUrl, imageView: self.carImageView)
            self.activityIndicator.stopAnimating()
        }.catch { error in print(error) }
    }
    
    static func nib() -> UINib {
        return UINib(nibName: String(describing: self), bundle: nil)
    }
}
