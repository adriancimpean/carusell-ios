//
//  FilteredAnnouncementsScreenController.swift
//  carusell-ios
//
//  Created by Adrian Cimpean on 01.06.2021.
//

import UIKit
import SwiftyJSON

class FilteredAnnouncementsScreenController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    public var filteredAnnouncementsJSON: JSON = JSON("")
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Int(self.filteredAnnouncementsJSON.count)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ExploreScreenCollectionViewCell", for: indexPath) as! ExploreScreenCollectionViewCell
        cell.configure(
            title: self.filteredAnnouncementsJSON[indexPath.row]["title"].stringValue,
            price: self.filteredAnnouncementsJSON[indexPath.row]["price"].stringValue,
            year: self.filteredAnnouncementsJSON[indexPath.row]["year"].stringValue,
            kilometers: self.filteredAnnouncementsJSON[indexPath.row]["kilometers"].stringValue,
            announcementId: self.filteredAnnouncementsJSON[indexPath.row]["announcement_id"].intValue,
            viewController: self
        )
        cell.contentView.layer.cornerRadius = 20.0
        cell.contentView.layer.borderWidth = 1.0
        cell.contentView.layer.borderColor = UIColor.clear.cgColor
        cell.contentView.layer.masksToBounds = false
        cell.layer.cornerRadius = 30.0
        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        cell.layer.shadowRadius = 10.0
        cell.layer.shadowOpacity = 1.0
        cell.layer.masksToBounds = false
        cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: cell.contentView.layer.cornerRadius).cgPath
                
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        if let announcementVC = UIStoryboard(name: "AnnouncementScreen", bundle: .main).instantiateInitialViewController() as? AnnouncementScreenController {
            announcementVC.announcementId = filteredAnnouncementsJSON[indexPath.row]["id"].intValue
            announcementVC.vehicleId = filteredAnnouncementsJSON[indexPath.row]["vehicle_id"].intValue
            announcementVC.annTitle = filteredAnnouncementsJSON[indexPath.row]["title"].stringValue
            announcementVC.annPrice = filteredAnnouncementsJSON[indexPath.row]["price"].stringValue
            announcementVC.annDescription = filteredAnnouncementsJSON[indexPath.row]["description"].stringValue
            navigationController?.pushViewController(announcementVC, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 190, height: 430)
    }
    
    private func configure() {
        collectionView.register(ExploreScreenCollectionViewCell.nib(), forCellWithReuseIdentifier: "ExploreScreenCollectionViewCell")
        let layout = UICollectionViewFlowLayout()
        collectionView.collectionViewLayout = layout
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        view.addGradient(color: .darkGray, startPoint: nil, endPoint: nil)
    }
}
