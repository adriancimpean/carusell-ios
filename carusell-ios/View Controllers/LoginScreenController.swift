//
//  LoginScreenController.swift
//  carusell-ios
//
//  Created by Adrian Cimpean on 18.04.2021.
//

import UIKit

class LoginScreenController: UIViewController {

    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var activtyIndicator: UIActivityIndicatorView!
    @IBAction func loginButtonPressed(_ sender: Any) {
        login()
    }
    @IBAction func registerButton(_ sender: Any) {
        guard let registerVC = UIStoryboard(name: "RegisterScreen", bundle: .main).instantiateInitialViewController() else { return }
        navigationController?.pushViewController(registerVC, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }
    
    private func configure() {
        activtyIndicator.hidesWhenStopped = true
        passwordTextField.layer.cornerRadius = passwordTextField.bounds.height / 2
        passwordTextField.layer.masksToBounds = true
        emailTextField.layer.cornerRadius = emailTextField.bounds.height / 2
        emailTextField.layer.masksToBounds = true
        loginButton.layer.cornerRadius = loginButton.bounds.height / 2
        loginButton.layer.masksToBounds = true
        view.addGradient(color: UIColor.systemYellow, startPoint: nil, endPoint: nil)
    }
    
    private func login() {
        let authManager = AuthManager()
        let email = emailTextField.text!
        let password = passwordTextField.text!
        
        authManager.login(email: email, password: password).done { success in
            self.activtyIndicator.startAnimating()
            guard success else {
                let snackbar = Snackbar(viewController: self, title: "Incorrect email or password", message: nil, severity: .error)
                snackbar.show()
                self.activtyIndicator.stopAnimating()
                return
            }
            if let exploreVC = UIStoryboard(name: "ExploreScreen", bundle: .main).instantiateInitialViewController() {
                let navigationController = CustomNavigationController()
                navigationController.viewControllers = [exploreVC]
                navigationController.modalPresentationStyle = .fullScreen
                self.present(navigationController, animated: true)
            }
            self.activtyIndicator.stopAnimating()
        }.catch { error in print(error) }
    }
}
