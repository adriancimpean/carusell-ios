//
//  SellScreenViewController.swift
//  carusell-ios
//
//  Created by Adrian Cimpean on 24.04.2021.
//

import UIKit

enum PickerViewType: Int {
    case make = 0
    case model = 1
    case bodyType = 2
    case fuelType = 3
    case transmissionType = 4
    case city = 5
}

enum ButtonActionType {
    case add
    case edit
}

class AnnouncementFormViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    public var announcementId: Int = 0
    private let imgurManager = ImgurManager()
    private let announcementManager = AnnouncementManager()

    var buttonActionType: ButtonActionType = .add
    
    //for editing announcement
    public var formTitle: String = "Add new announcement"
    public var announcementTitle: String = ""
    public var announcementYear: String = ""
    public var announcementKm: String = ""
    public var announcementEngineSize: String = ""
    public var announcementPrice: String = ""
    public var announcementEnginePower:String = ""
    public var announcementDescription: String = ""

    private var uploadedImageLink: String = ""
    private var createdAnnouncementId: Int = 0
    
    @IBOutlet weak var scrollView: UIScrollView!
    private var makesArray: [(Int, String)] = []
    private var modelsArray: [(Int, String)] = []
    private var bodyTypesArray: [(Int, String)] = []
    private var fuelTypesArray: [(Int, String)] = []
    private var transmissionTypesArray: [(Int, String)] = []
    private var citiesArray: [(Int, String)] = []
    
    @IBOutlet weak var makePickerView: UIPickerView!
    @IBOutlet weak var modelPickerView: UIPickerView!
    @IBOutlet weak var bodyTypePickerView: UIPickerView!
    @IBOutlet weak var fuelTypePickerView: UIPickerView!
    @IBOutlet weak var transmissionTypePickerView: UIPickerView!
    @IBOutlet weak var cityPickerView: UIPickerView!
    @IBOutlet weak var addPhotosButton: UIView!
    @IBOutlet weak var imageView: UIImageView!
    
    let imagePicker = UIImagePickerController()
    @IBAction func addPhotosButtonPressed(_ sender: Any) {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true)
    }
    
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var yearTextField: UITextField!
    @IBOutlet weak var kilometersTextField: UITextField!
    @IBOutlet weak var engineSizeTextField: UITextField!
    @IBOutlet weak var enginePowerTextField: UITextField!
    @IBOutlet weak var priceTextField: UITextField!
    
    @IBOutlet weak var descriptionTextView: UITextView!
    
    
    @IBOutlet weak var formTitleLabel: UILabel!
    @IBOutlet weak var postAnnouncementButton: UIButton!
    @IBAction func postAnnouncementButtonPressed(_ sender: Any) {
        switch buttonActionType {
        case .add:
           addNewAnnouncement()
        case .edit:
            editAnnouncement()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int { return 1 }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case PickerViewType.make.rawValue:
            return makesArray.count
        case PickerViewType.model.rawValue:
            return modelsArray.count
        case PickerViewType.bodyType.rawValue:
            return bodyTypesArray.count
        case PickerViewType.fuelType.rawValue:
            return fuelTypesArray.count
        case PickerViewType.transmissionType.rawValue:
            return transmissionTypesArray.count
        case PickerViewType.city.rawValue:
            return citiesArray.count
        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case PickerViewType.make.rawValue:
            return makesArray[row].1
        case PickerViewType.model.rawValue:
            return modelsArray[row].1
        case PickerViewType.bodyType.rawValue:
            return bodyTypesArray[row].1
        case PickerViewType.fuelType.rawValue:
            return fuelTypesArray[row].1
        case PickerViewType.transmissionType.rawValue:
            return transmissionTypesArray[row].1
        case PickerViewType.city.rawValue:
            return citiesArray[row].1
        default:
            return "Error"
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        switch pickerView.tag {
        case PickerViewType.make.rawValue:
            return NSAttributedString(string: makesArray[row].1, attributes: [NSAttributedString.Key.foregroundColor:UIColor.white])
        case PickerViewType.model.rawValue:
            return NSAttributedString(string: modelsArray[row].1, attributes: [NSAttributedString.Key.foregroundColor:UIColor.white])
        case PickerViewType.bodyType.rawValue:
            return NSAttributedString(string: bodyTypesArray[row].1, attributes: [NSAttributedString.Key.foregroundColor:UIColor.white])
        case PickerViewType.fuelType.rawValue:
            return NSAttributedString(string: fuelTypesArray[row].1, attributes: [NSAttributedString.Key.foregroundColor:UIColor.white])
        case PickerViewType.transmissionType.rawValue:
            return NSAttributedString(string: transmissionTypesArray[row].1, attributes: [NSAttributedString.Key.foregroundColor:UIColor.white])
        case PickerViewType.city.rawValue:
            return NSAttributedString(string: citiesArray[row].1, attributes: [NSAttributedString.Key.foregroundColor:UIColor.white])
        default:
            return NSAttributedString(string: "Error", attributes: [NSAttributedString.Key.foregroundColor:UIColor.white])
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if PickerViewType.make.rawValue == pickerView.tag {
            let makeId = makesArray[pickerView.selectedRow(inComponent: 0)].0
            announcementManager.getModelsForMakeId(requestType: RequestType.getModelsForMakeId(token: CurrentUser.shared.token, makeId: makeId)).done { success in
                self.modelsArray = success.map { json -> (Int, String) in
                    let id = json.1["id"].intValue
                    let name = json.1["name"].stringValue
                    return (id, name)
                }
                self.modelPickerView.reloadAllComponents()
            }.catch { error in print(error) }
        }
    }
    
    func populatePickerView(requestType : RequestType) {
        announcementManager.getData(requestType: requestType).done { success in
            let dataArray: [(Int, String)] = success.map { json -> (Int, String) in
                let id = json.1["id"].intValue
                let name = json.1["name"].stringValue
                return (id, name)
            }
            
            switch requestType {
            case .getMakes:
                self.makesArray = dataArray
                self.makePickerView.reloadAllComponents()
            case .getBodyTypes:
                self.bodyTypesArray = dataArray
                self.bodyTypePickerView.reloadAllComponents()
            case .getFuelTypes:
                self.fuelTypesArray = dataArray
                self.fuelTypePickerView.reloadAllComponents()
            case .getTransmissionTypes:
                self.transmissionTypesArray = dataArray
                self.transmissionTypePickerView.reloadAllComponents()
            case .getCities:
                self.citiesArray = dataArray.sorted { $0.1 < $1.1 }
                self.cityPickerView.reloadAllComponents()
            default:
                return
            }
        }.catch { error in print(error) }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            imageView.contentMode = .scaleAspectFit
            imageView.image = pickedImage
        }
        dismiss(animated: true)
    }
    
    private func getAnnouncementData() -> AnnouncementData {
        let announcementData = AnnouncementData(title: titleTextField.text ?? "",
                                                makeId: makesArray[makePickerView.selectedRow(inComponent: 0)].0,
                                                modelId: modelsArray[modelPickerView.selectedRow(inComponent: 0)].0,
                                                year: Int(yearTextField.text ?? "0") ?? 0,
                                                bodyTypeId: bodyTypesArray[bodyTypePickerView.selectedRow(inComponent: 0)].0,
                                                kilometers: Int(kilometersTextField.text ?? "0") ?? 0,
                                                fuelTypeId: fuelTypesArray[fuelTypePickerView.selectedRow(inComponent: 0)].0,
                                                transmissionTypeId: transmissionTypesArray[transmissionTypePickerView.selectedRow(inComponent: 0)].0,
                                                engineSize: Int(engineSizeTextField.text ?? "0") ?? 0,
                                                enginePower: Int(enginePowerTextField.text ?? "0") ?? 0,
                                                price: Int(priceTextField.text ?? "0") ?? 0,
                                                cityId: citiesArray[cityPickerView.selectedRow(inComponent: 0)].0,
                                                description: descriptionTextView.text ?? "No description provided")
        return announcementData
    }
    
    private func addNewAnnouncement() {
        self.announcementManager.createAnnouncement(announcementData: self.getAnnouncementData(), image: self.imageView.image!).done { [weak self] succes in
            guard let self = self else { return }
            self.createdAnnouncementId = succes["announcement"]["id"].intValue
            let snackbar = Snackbar(viewController: self, title: succes["message"].stringValue, message: nil, severity: .other)
            snackbar.show()
        }.catch { error in print(error) }

        self.imgurManager.uploadImage(image: self.imageView.image!)?.done { [weak self] success in
            guard let self = self else { return }
            self.uploadedImageLink = success["data"]["link"].stringValue
           
            self.announcementManager.saveImageToDatabase(annoucementId: self.createdAnnouncementId, photoUrl: self.uploadedImageLink).done { success in
            }.catch { error in print(error) }
        }.catch { error in print(error) }
    }
    
    private func editAnnouncement() {
        self.announcementManager.editAnnouncement(id: announcementId, announcementData: self.getAnnouncementData()).done { [weak self] success in
            guard let self = self else { return }
            let snackbar = Snackbar(viewController: self, title: success["message"].stringValue, message: nil, severity: .success)
            snackbar.show()
        }.catch { error in print(error) }
        
        self.imgurManager.uploadImage(image: self.imageView.image!)?.done { [weak self] success in
            guard let self = self else { return }
            self.uploadedImageLink = success["data"]["link"].stringValue
            self.announcementManager.updateAnnouncementImage(announcementId: self.announcementId, photoUrl: self.uploadedImageLink).done { success in
            }.catch { error in print(error) }
        }.catch { error in print(error) }
    }
    
    private func configure() {
        postAnnouncementButton.layer.cornerRadius = postAnnouncementButton.bounds.height / 2
        postAnnouncementButton.layer.masksToBounds = true
        
        let pickerViewsArray = [makePickerView, modelPickerView, bodyTypePickerView, fuelTypePickerView, transmissionTypePickerView, cityPickerView]
        populatePickerView(requestType: RequestType.getMakes(token: CurrentUser.shared.token))
        populatePickerView(requestType: RequestType.getBodyTypes(token: CurrentUser.shared.token))
        populatePickerView(requestType: RequestType.getFuelTypes(token: CurrentUser.shared.token))
        populatePickerView(requestType: RequestType.getTransmissionTypes(token: CurrentUser.shared.token))
        populatePickerView(requestType: RequestType.getCities(token: CurrentUser.shared.token))
        pickerViewsArray.forEach { pickerView in
            pickerView?.dataSource = self
            pickerView?.delegate = self
        }
        descriptionTextView.font = UIFont(name: "Poppins-Medium", size: 20)
        descriptionTextView.layer.cornerRadius = 25
        descriptionTextView.layer.masksToBounds = true
        descriptionTextView.layer.borderColor = UIColor.systemYellow.cgColor
        descriptionTextView.layer.borderWidth = 2
        imagePicker.delegate = self
        scrollView.addGradient(color: UIColor.darkGray, startPoint: nil, endPoint: nil)
        view.addGradient(color: UIColor.darkGray, startPoint: nil, endPoint: nil)
        
        formTitleLabel.text = formTitle
        titleTextField.text = announcementTitle
        yearTextField.text = announcementYear
        kilometersTextField.text = announcementKm
        engineSizeTextField.text = announcementEngineSize
        enginePowerTextField.text = announcementEnginePower
        priceTextField.text = announcementPrice
        descriptionTextView.text = announcementDescription
        postAnnouncementButton.setTitle(formTitle, for: .normal)
    }
}
