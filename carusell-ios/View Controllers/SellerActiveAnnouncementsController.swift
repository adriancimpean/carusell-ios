//
//  SellerActiveAnnouncementsController.swift
//  carusell-ios
//
//  Created by Adrian Cimpean on 17.06.2021.
//

import UIKit
import SwiftyJSON

class SellerActiveAnnouncementsController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    var announcements: JSON = []
    let announcementManager = AnnouncementManager()
    public var sellerFirstName: String = ""
    public var sellerLastName: String = ""
    public var sellerCityName: String?
    public var userId: Int = 0
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        announcementManager.getUserActiveAnnouncements(userId: userId).done { success in
            self.announcements = success
            self.collectionView.reloadData()
        }.catch { error in print(error) }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Int(self.announcements.count)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ExploreScreenCollectionViewCell", for: indexPath) as! ExploreScreenCollectionViewCell
        cell.configure(
            title: self.announcements[indexPath.row]["title"].stringValue,
            price: self.announcements[indexPath.row]["price"].stringValue,
            year: self.announcements[indexPath.row]["year"].stringValue,
            kilometers: self.announcements[indexPath.row]["kilometers"].stringValue,
            announcementId: self.announcements[indexPath.row]["id"].intValue,
            viewController: self
        )
        cell.contentView.layer.cornerRadius = 20.0
        cell.contentView.layer.borderWidth = 1.0
        cell.contentView.layer.borderColor = UIColor.clear.cgColor
        cell.contentView.layer.masksToBounds = false
        cell.layer.cornerRadius = 30.0
        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        cell.layer.shadowRadius = 10.0
        cell.layer.shadowOpacity = 1.0
        cell.layer.masksToBounds = false
        cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: cell.contentView.layer.cornerRadius).cgPath
                
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        if let announcementVC = UIStoryboard(name: "AnnouncementScreen", bundle: .main).instantiateInitialViewController() as? AnnouncementScreenController {
            announcementVC.announcementId = announcements[indexPath.row]["id"].intValue
            announcementVC.vehicleId = announcements[indexPath.row]["vehicle_id"].intValue
            announcementVC.annTitle = announcements[indexPath.row]["title"].stringValue
            announcementVC.annPrice = announcements[indexPath.row]["price"].stringValue
            announcementVC.annDescription = announcements[indexPath.row]["description"].stringValue
            announcementVC.annCity = announcements[indexPath.row]["cityName"].stringValue
            announcementVC.sellerFirstName = announcements[indexPath.row]["first_name"].stringValue
            navigationController?.pushViewController(announcementVC, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 190, height: 430)
    }
    
    private func configure() {
        collectionView.register(ExploreScreenCollectionViewCell.nib(), forCellWithReuseIdentifier: "ExploreScreenCollectionViewCell")
        nameLabel.text = sellerFirstName + " " + sellerLastName
        cityLabel.text = sellerCityName
        let layout = UICollectionViewFlowLayout()
        collectionView.collectionViewLayout = layout
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        let background = UIView(frame: collectionView.bounds)
        background.addGradient(color: UIColor.darkGray, startPoint: nil, endPoint: nil)
        collectionView.backgroundView = background
        view.addGradient(color: UIColor.systemYellow, startPoint: nil, endPoint: nil)
    }
}
