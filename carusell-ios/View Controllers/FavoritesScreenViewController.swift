//
//  FavoritesScreenViewController.swift
//  carusell-ios
//
//  Created by Adrian Cimpean on 20.05.2021.
//

import UIKit
import SwiftyJSON

class FavoritesScreenViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, FavoritesAnnoucementsCellDelegate {
    
    var favoriteAnnouncements: JSON = []
    let announcementManager = AnnouncementManager()

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }
    
    override func viewDidAppear(_ animated: Bool) {
       fetchFavoriteAnnouncements()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let announcementVC = UIStoryboard(name: "AnnouncementScreen", bundle: .main).instantiateInitialViewController() as? AnnouncementScreenController {
            announcementVC.announcementId = favoriteAnnouncements[indexPath.row]["announcement_id"].intValue
            announcementVC.vehicleId = favoriteAnnouncements[indexPath.row]["vehicle_id"].intValue
            announcementVC.annTitle = favoriteAnnouncements[indexPath.row]["title"].stringValue
            announcementVC.annPrice = favoriteAnnouncements[indexPath.row]["vehicle_price"].stringValue
            announcementVC.annDescription = favoriteAnnouncements[indexPath.row]["description"].stringValue
            navigationController?.pushViewController(announcementVC, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "FavoritesTableViewCell", for: indexPath) as? FavoritesTableViewCell else { return UITableViewCell() }
        cell.configure(title: favoriteAnnouncements[indexPath.row]["title"].stringValue,
                       favoriteId: favoriteAnnouncements[indexPath.row]["favorite_id"].intValue,
                       announcementId: favoriteAnnouncements[indexPath.row]["announcement_id"].intValue,
                       viewController: self)
        cell.delegate = self
        return cell
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        Int(self.favoriteAnnouncements.count)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.rowHeight
    }
    
    func removeFromFavoritesButtonPressed(cell: UITableViewCell) {
        fetchFavoriteAnnouncements()
    }
    
    private func fetchFavoriteAnnouncements() {
        announcementManager.getUserFavoriteAnnouncements(userId: CurrentUser.shared.userId).done { success in
            self.favoriteAnnouncements = success
            self.tableView.reloadData()
        }.catch { error in print(error) }
    }
    
    private func configure() {
        tableView.register(FavoritesTableViewCell.nib(), forCellReuseIdentifier: "FavoritesTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.clipsToBounds = true
        let background = UIView(frame: tableView.bounds)
        background.addGradient(color: UIColor.darkGray, startPoint: nil, endPoint: nil)
        tableView.backgroundView = background
    }
}
