//
//  AccountScreenController.swift
//  carusell-ios
//
//  Created by Adrian Cimpean on 22.04.2021.
//

import UIKit
import KeychainSwift

class AccountScreenController: UIViewController, ProfileScreenDelegate {
    let keychain = KeychainSwift()

    @IBOutlet weak var activeAnnouncementsButton: UIButton!
    @IBOutlet weak var logOutButton: UIButton!
    @IBOutlet weak var changePasswordButton: UIButton!
    @IBOutlet weak var editProfileButton: UIButton!
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBAction func ActiveAnnoucementsButtonPressed(_ sender: Any) {
        if let activeAnnoucementsVC = UIStoryboard(name: "ActiveAnnouncementsScreen", bundle: .main).instantiateInitialViewController() {
            navigationController?.pushViewController(activeAnnoucementsVC, animated: true)
            navigationController?.modalPresentationStyle = .fullScreen
        }
    }
    @IBAction func logOutButtonPressed(_ sender: Any) {
        keychain.clear()
        if let loginScreenVC = UIStoryboard(name: "LoginScreen", bundle: .main).instantiateInitialViewController(){
            let authNavigationController = CustomNavigationController()
            authNavigationController.viewControllers=[loginScreenVC]
            authNavigationController.modalPresentationStyle = .fullScreen
            self.present(authNavigationController, animated: true)
        }
    }
    @IBAction func changePasswordButtonPressed(_ sender: Any) {
    }
    @IBAction func editProfileButtonPressed(_ sender: Any) {
        if let profileVC = UIStoryboard(name: "EditProfileScreen", bundle: .main).instantiateInitialViewController() as? ProfileScreenController {
            navigationController?.pushViewController(profileVC, animated: true)
            navigationController?.modalPresentationStyle = .fullScreen
            profileVC.delegate = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userNameLabel.text = "\(CurrentUser.shared.firstName) \(CurrentUser.shared.lastName)"
        view.addGradient(color: UIColor.darkGray, startPoint: nil, endPoint: nil)
        view.layoutIfNeeded()
    }
    
    override func viewDidLayoutSubviews() {
        [activeAnnouncementsButton, logOutButton, changePasswordButton, editProfileButton].forEach { button in
            button?.layer.cornerRadius = (button?.frame.height ?? 0) / 2
            button?.layer.masksToBounds = true
        }
    }
    func saveChangesButtonPressed() {
        userNameLabel.text = CurrentUser.shared.firstName + " " + CurrentUser.shared.lastName
    }
}
