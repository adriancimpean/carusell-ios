//
//  FilterScreenController.swift
//  carusell-ios
//
//  Created by Adrian Cimpean on 29.05.2021.
//

import UIKit
import SwiftRangeSlider

enum FilterPickerViewType: Int {
    case make = 0
    case model = 1
    case bodyType = 2
    case fuelType = 3
    case transmissionType = 4
    case city = 5
}

class FilterScreenController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UIImagePickerControllerDelegate {
    private let announcementManager = AnnouncementManager()
    private var makesArray: [(Int, String)] = []
    private var modelsArray: [(Int, String)] = []
    private var bodyTypesArray: [(Int, String)] = []
    private var fuelTypesArray: [(Int, String)] = []
    private var transmissionTypesArray: [(Int, String)] = []
    private var citiesArray: [(Int, String)] = []
    private var filters: [String] = []
    
    @IBOutlet weak var makesPickerView: UIPickerView!
    @IBOutlet weak var modelsPickerView: UIPickerView!
    @IBOutlet weak var bodyTypesPickerView: UIPickerView!
    @IBOutlet weak var fuelTypesPickerView: UIPickerView!
    @IBOutlet weak var transmissionTypesPickerView: UIPickerView!
    @IBOutlet weak var citiesPickerView: UIPickerView!
    @IBOutlet weak var yearRangeSelector: RangeSlider!
    @IBOutlet weak var kilometersFromTextField: UITextField!
    @IBOutlet weak var kilometersToTextField: UITextField!
    @IBOutlet weak var priceFromTextField: UITextField!
    @IBOutlet weak var priceToTextField: UITextField!
    @IBOutlet weak var filterButton: UIButton!
    @IBOutlet weak var titleOrDescriptionTextField: UITextField!
    @IBAction func filterButtonPressed(_ sender: Any) {
        getFilters()
        announcementManager.filterAnnouncements(filters: filters).done { success in
            if let filteredVC = UIStoryboard(name: "FilteredAnnouncementsScreen", bundle: .main).instantiateInitialViewController() as? FilteredAnnouncementsScreenController {
                filteredVC.filteredAnnouncementsJSON = success
                self.navigationController?.pushViewController(filteredVC, animated: true)
                self.navigationController?.modalPresentationStyle = .fullScreen
            }
        }.catch { error in print(error) }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int { return 1 }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case FilterPickerViewType.make.rawValue:
            return makesArray.count
        case FilterPickerViewType.model.rawValue:
            return modelsArray.count
        case FilterPickerViewType.bodyType.rawValue:
            return bodyTypesArray.count
        case FilterPickerViewType.fuelType.rawValue:
            return fuelTypesArray.count
        case FilterPickerViewType.transmissionType.rawValue:
            return transmissionTypesArray.count
        case FilterPickerViewType.city.rawValue:
            return citiesArray.count
        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case FilterPickerViewType.make.rawValue:
            return makesArray[row].1
        case FilterPickerViewType.model.rawValue:
            return modelsArray[row].1
        case FilterPickerViewType.bodyType.rawValue:
            return bodyTypesArray[row].1
        case FilterPickerViewType.fuelType.rawValue:
            return fuelTypesArray[row].1
        case FilterPickerViewType.transmissionType.rawValue:
            return transmissionTypesArray[row].1
        case FilterPickerViewType.city.rawValue:
            return citiesArray[row].1
        default:
            return "Error"
        }
    }
        
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        switch pickerView.tag {
        case FilterPickerViewType.make.rawValue:
            return NSAttributedString(string: makesArray[row].1, attributes: [NSAttributedString.Key.foregroundColor:UIColor.white])
        case FilterPickerViewType.model.rawValue:
            return NSAttributedString(string: modelsArray[row].1, attributes: [NSAttributedString.Key.foregroundColor:UIColor.white])
        case FilterPickerViewType.bodyType.rawValue:
            return NSAttributedString(string: bodyTypesArray[row].1, attributes: [NSAttributedString.Key.foregroundColor:UIColor.white])
        case FilterPickerViewType.fuelType.rawValue:
            return NSAttributedString(string: fuelTypesArray[row].1, attributes: [NSAttributedString.Key.foregroundColor:UIColor.white])
        case FilterPickerViewType.transmissionType.rawValue:
            return NSAttributedString(string: transmissionTypesArray[row].1, attributes: [NSAttributedString.Key.foregroundColor:UIColor.white])
        case FilterPickerViewType.city.rawValue:
            return NSAttributedString(string: citiesArray[row].1, attributes: [NSAttributedString.Key.foregroundColor:UIColor.white])
        default:
            return NSAttributedString(string: "Error", attributes: [NSAttributedString.Key.foregroundColor:UIColor.white])
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if FilterPickerViewType.make.rawValue == pickerView.tag {
            let makeId = makesArray[pickerView.selectedRow(inComponent: 0)].0
            announcementManager.getModelsForMakeId(requestType: RequestType.getModelsForMakeId(token: CurrentUser.shared.token, makeId: makeId)).done { success in
                self.modelsArray = success.map { json -> (Int, String) in
                    let id = json.1["id"].intValue
                    let name = json.1["name"].stringValue
                    return (id, name)
                }
                self.modelsArray.insert((0, "None"), at: 0)
                self.modelsPickerView.reloadAllComponents()
            }.catch { error in print(error) }
        }
    }
    
    func populatePickerView(requestType : RequestType) {
        announcementManager.getData(requestType: requestType).done { success in
            var dataArray = success.map { json -> (Int, String) in
                let id = json.1["id"].intValue
                let name = json.1["name"].stringValue
                return (id, name)
            }
            dataArray.insert((0, "None"), at: 0)
            
            switch requestType {
            case .getMakes:
                self.makesArray = dataArray
                self.makesPickerView.reloadAllComponents()
            case .getBodyTypes:
                self.bodyTypesArray = dataArray
                self.bodyTypesPickerView.reloadAllComponents()
            case .getFuelTypes:
                self.fuelTypesArray = dataArray
                self.fuelTypesPickerView.reloadAllComponents()
            case .getTransmissionTypes:
                self.transmissionTypesArray = dataArray
                self.transmissionTypesPickerView.reloadAllComponents()
            case .getCities:
                self.citiesArray = dataArray.sorted { $0.1 < $1.1 }
                self.citiesArray.insert((99999, "None"), at: 0)
                self.citiesPickerView.reloadAllComponents()
            default:
                return
            }
        }.catch { error in print(error) }
    }
    
    private func getFilters() {
        filters.removeAll()
        if (makesArray[makesPickerView.selectedRow(inComponent: 0)].1 != "None") {
            filters.append("make_id=\(makesArray[makesPickerView.selectedRow(inComponent: 0)].0)")
        }
        if (modelsArray.count != 0) {
            if (modelsArray[modelsPickerView.selectedRow(inComponent: 0)].1 != "None") {
                filters.append("model_id=\(modelsArray[modelsPickerView.selectedRow(inComponent: 0)].0)")
            }
        }
        if (bodyTypesArray[bodyTypesPickerView.selectedRow(inComponent: 0)].1 != "None") {
            filters.append("bodyType_id=\(bodyTypesArray[bodyTypesPickerView.selectedRow(inComponent: 0)].0)")
        }
        if (fuelTypesArray[fuelTypesPickerView.selectedRow(inComponent: 0)].1 != "None") {
            filters.append("fuelType_id=\(fuelTypesArray[fuelTypesPickerView.selectedRow(inComponent: 0)].0)")
        }
//        if (transmissionTypesArray[transmissionTypesPickerView.selectedRow(inComponent: 0)].1 != "None") {
//            filters.append("transmissionType_id=\(transmissionTypesArray[transmissionTypesPickerView.selectedRow(inComponent: 0)].0)")
//        }
        if (citiesArray[citiesPickerView.selectedRow(inComponent: 0)].1 != "None") {
            filters.append("city_id=\(citiesArray[citiesPickerView.selectedRow(inComponent: 0)].0)")
        }
        if(!kilometersFromTextField.text!.isEmpty && !kilometersToTextField.text!.isEmpty) {
            filters.append("km:from=\(kilometersFromTextField.text ?? "0")")
            filters.append("km:to=\(kilometersToTextField.text ?? "99999999")")
        }
        if(!priceFromTextField.text!.isEmpty && !priceToTextField.text!.isEmpty) {
            filters.append("price:from=\(priceFromTextField.text ?? "0")")
            filters.append("price:to=\(priceToTextField.text ?? "99999999")")
        }
        if(!titleOrDescriptionTextField.text!.isEmpty) {
            filters.append("text=\(titleOrDescriptionTextField.text ?? "")")
        }
        filters.append("year:from=\(yearRangeSelector.lowerValue)")
        filters.append("year:to=\(yearRangeSelector.upperValue)")
    }
    
    private func configure() {
        view.addGradient(color: .darkGray, startPoint: nil, endPoint: nil)
        filterButton.layer.cornerRadius = filterButton.bounds.height / 2
        filterButton.layer.masksToBounds = true
        [makesPickerView, modelsPickerView, bodyTypesPickerView, fuelTypesPickerView, transmissionTypesPickerView, citiesPickerView].forEach {  pickerView in
            pickerView?.dataSource = self
            pickerView?.delegate = self
        }
       populatePickerView(requestType: RequestType.getMakes(token: CurrentUser.shared.token))
       populatePickerView(requestType: RequestType.getBodyTypes(token: CurrentUser.shared.token))
       populatePickerView(requestType: RequestType.getFuelTypes(token: CurrentUser.shared.token))
       populatePickerView(requestType: RequestType.getTransmissionTypes(token: CurrentUser.shared.token))
       populatePickerView(requestType: RequestType.getCities(token: CurrentUser.shared.token))
    }
}
