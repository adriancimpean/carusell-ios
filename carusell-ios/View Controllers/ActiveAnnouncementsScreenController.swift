//
//  ActiveaAnnouncementsScreenController.swift
//  carusell-ios
//
//  Created by Adrian Cimpean on 30.04.2021.
//

import UIKit
import SwiftyJSON

class ActiveAnnouncementsScreenController: UIViewController, UITableViewDelegate, UITableViewDataSource, ActiveAnnouncementsCellDelegate {

    var activeAnnouncements: JSON = []
    let announcementManager = AnnouncementManager()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(ActiveAnnouncementsTableViewCell.nib(), forCellReuseIdentifier: "ActiveAnnouncementsScreenCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.clipsToBounds = true

        announcementManager.getUserActiveAnnouncements(userId: CurrentUser.shared.userId).done { success in
            self.activeAnnouncements = success
            self.tableView.reloadData()
        }.catch { error in print(error) }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let announcementVC = UIStoryboard(name: "AnnouncementScreen", bundle: .main).instantiateInitialViewController() as? AnnouncementScreenController {
            announcementVC.announcementId = activeAnnouncements[indexPath.row]["id"].intValue
            announcementVC.vehicleId = activeAnnouncements[indexPath.row]["vehicle_id"].intValue
            announcementVC.annTitle = activeAnnouncements[indexPath.row]["title"].stringValue
            announcementVC.annPrice = activeAnnouncements[indexPath.row]["price"].stringValue
            announcementVC.annDescription = activeAnnouncements[indexPath.row]["description"].stringValue
            announcementVC.annCity = activeAnnouncements[indexPath.row]["cityName"].stringValue
            announcementVC.sellerFirstName = activeAnnouncements[indexPath.row]["first_name"].stringValue
            announcementVC.sellerLastName = activeAnnouncements[indexPath.row]["last_name"].stringValue
            announcementVC.userId = activeAnnouncements[indexPath.row]["user_id"].intValue
            announcementVC.sellerPhoneNumber = activeAnnouncements[indexPath.row]["phone"].stringValue
            announcementVC.sellerEmail = activeAnnouncements[indexPath.row]["email"].stringValue
            navigationController?.pushViewController(announcementVC, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ActiveAnnouncementsScreenCell", for: indexPath) as? ActiveAnnouncementsTableViewCell else { return UITableViewCell() }
        
        cell.configure(
            viewController: self,
            announcementId: activeAnnouncements[indexPath.row]["id"].intValue, 
            title: activeAnnouncements[indexPath.row]["title"].stringValue,
            announcement: activeAnnouncements[indexPath.row])
            
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Int(self.activeAnnouncements.count)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.rowHeight
    }
    
    func deleteButtonPressed(cell: UITableViewCell) {
        announcementManager.getUserActiveAnnouncements(userId: CurrentUser.shared.userId).done { success in
            self.activeAnnouncements = success
            self.tableView.reloadData()
        }.catch { error in print(error) }
    }
}
