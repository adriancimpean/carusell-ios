//
//  AnnouncementScreenController.swift
//  carusell-ios
//
//  Created by Adrian Cimpean on 21.04.2021.
//

import UIKit
import SwiftyJSON

class AnnouncementScreenController: UIViewController {
    let announcementManager = AnnouncementManager()

    public var vehicleId: Int = 0
    public var annTitle: String?
    public var annPrice: String?
    public var annDescription: String?
    public var annCity: String?
    public var sellerPhoneNumber: String = ""
    public var sellerEmail: String = ""
    public var sellerFirstName: String = "N/A"
    public var sellerLastName: String = "N/A"
    public var announcementId: Int = 0
    public var userId: Int = 0
    private var vehicleDetails: JSON?
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet public weak var carImageView: UIImageView!
    @IBOutlet weak var announcementTitle: UILabel!
    @IBOutlet weak var bodyTypeLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var kmLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var makeLabel: UILabel!
    @IBOutlet weak var engineSizeLabel: UILabel!
    @IBOutlet weak var modelLabel: UILabel!
    @IBOutlet weak var transmissionTypeLabel: UILabel!
    @IBOutlet weak var enginePowerLabel: UILabel!
    @IBOutlet weak var fuelTypeLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var callButton: UIButton!
    @IBOutlet weak var mailButton: UIButton!
    @IBAction func callButtonPressed(_ sender: Any) {
        callButton.setTitle(sellerPhoneNumber, for: .normal)
    }
    @IBAction func mailButtonPressed(_ sender: Any) {
        mailButton.setTitle(sellerEmail, for: .normal)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        configure()
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        guard let tappedImage = tapGestureRecognizer.view as? UIImageView else { return }
        let fullscreenImage = UIImageView(image: tappedImage.image)
        fullscreenImage.frame = tappedImage.frame
        fullscreenImage.backgroundColor = .black
        fullscreenImage.contentMode = .scaleAspectFit
        fullscreenImage.isUserInteractionEnabled = true
        view.addSubview(fullscreenImage)
        self.navigationController?.isNavigationBarHidden = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
        fullscreenImage.addGestureRecognizer(tap)
        
        UIView.animate(withDuration: 0.5) {
            fullscreenImage.frame = UIScreen.main.bounds
        }
    }
    
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        self.navigationController?.isNavigationBarHidden = false
        self.tabBarController?.tabBar.isHidden = false
        sender.view?.removeFromSuperview()
    }
    
    @objc func nameLabelTapped(_ sender: UITapGestureRecognizer) {
        if let sellerActiveAnnouncementsVC = UIStoryboard(name: "SellerActiveAnnouncements", bundle: .main).instantiateInitialViewController() as? SellerActiveAnnouncementsController{
            sellerActiveAnnouncementsVC.sellerFirstName = self.sellerFirstName
            sellerActiveAnnouncementsVC.sellerLastName = self.sellerLastName
            sellerActiveAnnouncementsVC.sellerCityName = self.annCity
            sellerActiveAnnouncementsVC.userId = self.userId
            navigationController?.pushViewController(sellerActiveAnnouncementsVC, animated: true)
        }
    }
    
    private func loadData() {
        announcementManager.getVehicle(id: String(vehicleId)).done { [weak self] success in
            guard let self = self else { return }
            self.vehicleDetails = success
            self.announcementTitle.text = self.annTitle
            self.priceLabel.text = "\(self.annPrice ?? "0") eur"
            self.kmLabel.text = self.vehicleDetails?[0]["kilometers"].stringValue
            self.yearLabel.text = self.vehicleDetails?[0]["year"].stringValue
            self.engineSizeLabel.text = self.vehicleDetails?[0]["engine_size"].stringValue
            self.bodyTypeLabel.text = self.vehicleDetails?[0]["bodyType"].stringValue
            self.makeLabel.text = self.vehicleDetails?[0]["make"].stringValue
            self.modelLabel.text = self.vehicleDetails?[0]["model"].stringValue
            self.engineSizeLabel.text = self.vehicleDetails?[0]["engine_size"].stringValue
            self.transmissionTypeLabel.text = self.vehicleDetails?[0]["transmissionType"].stringValue
            self.enginePowerLabel.text = "\(self.vehicleDetails?[0]["power"].stringValue ?? "n/a") hp"
            self.fuelTypeLabel.text = self.vehicleDetails?[0]["fuel_type"].stringValue
            self.descriptionLabel.text = self.annDescription
            self.cityLabel.text = self.annCity
            self.nameLabel.text = self.sellerFirstName
            self.activityIndicator.stopAnimating()
        }.catch { error in print(error) }
        
        announcementManager.getAnnouncementImages(announcementId: announcementId).done { success in
            let photoUrl = success[0]["photo_url"].stringValue
            Utils.loadImage(imageUrl: photoUrl, imageView: self.carImageView)
        }.catch { error in print(error) }
    }
    
    private func configure() {
        activityIndicator.startAnimating()
        scrollView.addGradient(color: UIColor.systemYellow, startPoint: CGPoint(x: 0, y: 0), endPoint: CGPoint(x: 1, y: 1))
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        carImageView.addGestureRecognizer(tapGestureRecognizer)
        let tap = UITapGestureRecognizer(target: self, action: #selector(AnnouncementScreenController.nameLabelTapped))
               nameLabel.isUserInteractionEnabled = true
               nameLabel.addGestureRecognizer(tap)
    }
}
