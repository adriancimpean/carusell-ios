//
//  RegisterScreenController.swift
//  carusell-ios
//
//  Created by Adrian Cimpean on 18.04.2021.
//

import UIKit

class RegisterScreenController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    let userManager = UserManager()
    let announcementManager = AnnouncementManager()
    var citiesArray: [(Int, String)] = []
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var passwordConfirmTextField: UITextField!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var cityPickerView: UIPickerView!
    @IBOutlet weak var registerButton: UIButton!
    
    @IBAction func registerButtonPressed(_ sender: Any) {
       register()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        announcementManager.getData(requestType: RequestType.getCities(token: CurrentUser.shared.token)).done { success in
            let dataArray: [(Int, String)] = success.map { json -> (Int, String) in
                let id = json.1["id"].intValue
                let name = json.1["name"].stringValue
                return (id, name)
            }
            self.citiesArray = dataArray.sorted { $0.1 < $1.1 }
            self.cityPickerView.reloadAllComponents()
        }.catch { error in print(error) }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return citiesArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        return NSAttributedString(string: citiesArray[row].1, attributes: [NSAttributedString.Key.foregroundColor:UIColor.white])
    }
    
    private func configure() {
        cityPickerView.delegate = self
        cityPickerView.dataSource = self
        [emailTextField, passwordTextField, passwordConfirmTextField, firstNameTextField, lastNameTextField, phoneTextField].forEach { textField in
            textField?.layer.cornerRadius = emailTextField.bounds.height / 2
            textField?.layer.masksToBounds = true
        }
        registerButton.layer.cornerRadius = registerButton.bounds.height / 2
        registerButton.layer.masksToBounds = true
        view.addGradient(color: UIColor.systemYellow, startPoint: nil, endPoint: nil)
    }
    
    private func register() {
        let authManager = AuthManager()
        let email = emailTextField.text!
        let password = passwordTextField.text!
        let passwordConfirmation = passwordConfirmTextField.text!
        let firstName = firstNameTextField.text!
        let lastName = lastNameTextField.text!
        let phone = phoneTextField.text!
        authManager.register(email: email, password: password, passwordConfirmation: passwordConfirmation, firstName: firstName, lastName: lastName, phone: phone, cityId: citiesArray[cityPickerView.selectedRow(inComponent: 0)].0 ).done { success in
            guard success else {
                let snackbar = Snackbar(viewController: self, title: "Something went wrong, please try again later", message: nil, severity: .error)
                snackbar.show()
                return
            }
            let registerSuccessAlert = UIAlertController(title: "Success", message: "Successfully registered!", preferredStyle: .alert)
            registerSuccessAlert.addAction(UIAlertAction(title: "Take me to app", style: .default) { _ in
                authManager.login(email: email, password: password).done { success in
                    guard success else {
                        let snackbar = Snackbar(viewController: self, title: "Something went wrong.", message: nil, severity: .error)
                        snackbar.show()
                        return
                    }
                    if let exploreVC = UIStoryboard(name: "ExploreScreen", bundle: .main).instantiateInitialViewController() {
                        let navigationController = CustomNavigationController()
                        navigationController.viewControllers = [exploreVC]
                        navigationController.modalPresentationStyle = .fullScreen
                        self.present(navigationController, animated: true)
                    }
                }.catch { error in print(error) }
            })
            self.present(registerSuccessAlert, animated: true)
            authManager.addCredentialsToKeychain(email: email, password: password)
        }.catch { error in print(error) }
    }
}
