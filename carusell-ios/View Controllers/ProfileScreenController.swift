//
//  ProfileScreenController.swift
//  carusell-ios
//
//  Created by Adrian Cimpean on 01.05.2021.
//

import UIKit

enum ResponseMessages: String {
    case successfullyUpdated = "User successfully updated"
}

public protocol ProfileScreenDelegate: AnyObject {
    func saveChangesButtonPressed()
}

class ProfileScreenController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    weak var delegate: ProfileScreenDelegate?
    let announcementManager = AnnouncementManager()
    let userManager = UserManager()
    var citiesArray: [(Int,String)] = []
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var cityPickerView: UIPickerView!
    @IBOutlet weak var saveChangesButton: UIButton!
    @IBAction func saveChangesButtonPressed(_ sender: Any) {
        let user = User(
            userId: CurrentUser.shared.userId,
            email: emailTextField.text ?? CurrentUser.shared.email,
            firstName: firstNameTextField.text ?? CurrentUser.shared.firstName,
            lastName: lastNameTextField.text ?? CurrentUser.shared.lastName,
            phoneNumber: phoneTextField.text ?? CurrentUser.shared.phone,
            cityId: citiesArray[cityPickerView.selectedRow(inComponent: 0)].0)
        
        userManager.updateUser(user: user, userId: CurrentUser.shared.userId).done { success in
            if(success["message"].stringValue == ResponseMessages.successfullyUpdated.rawValue) {
                let snackbar = Snackbar(viewController: self, title: success["message"].stringValue, message: nil, severity: .success)
                snackbar.show()
                CurrentUser.shared.setCurrentUser(userId: CurrentUser.shared.userId, token: CurrentUser.shared.token, firstName: success["0"]["first_name"].stringValue, lastName: success["0"]["last_name"].stringValue, email: success["0"]["email"].stringValue, phone: success["0"]["phone"].stringValue, cityId: success["0"]["city_id"].intValue)
            } else {
                let snackbar = Snackbar(viewController: self, title: success["message"].stringValue, message: nil, severity: .error)
                snackbar.show()
            }
            self.delegate?.saveChangesButtonPressed()
        }.catch { error in print(error) }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        announcementManager.getData(requestType: RequestType.getCities(token: CurrentUser.shared.token)).done { success in
            let dataArray: [(Int, String)] = success.map { json -> (Int, String) in
                let id = json.1["id"].intValue
                let name = json.1["name"].stringValue
                return (id, name)
            }
            self.citiesArray = dataArray.sorted { $0.1 < $1.1 }
            self.cityPickerView.reloadAllComponents()
        }.catch { error in print(error) }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int { return 1 }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return citiesArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        return NSAttributedString(string: citiesArray[row].1, attributes: [NSAttributedString.Key.foregroundColor:UIColor.white])
    }
    
    private func configure() {
        cityPickerView.delegate = self
        cityPickerView.dataSource = self
        saveChangesButton.layer.cornerRadius = saveChangesButton.bounds.height / 2
        saveChangesButton.layer.masksToBounds = true
        emailTextField.text = CurrentUser.shared.email
        firstNameTextField.text = CurrentUser.shared.firstName
        lastNameTextField.text = CurrentUser.shared.lastName
        phoneTextField.text = CurrentUser.shared.phone
        view.addGradient(color: UIColor.darkGray, startPoint: nil, endPoint: nil)
        
    }
}
