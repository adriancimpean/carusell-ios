//
//  SplashScreenController.swift
//  carusell-ios
//
//  Created by Adrian Cimpean on 18.04.2021.
//

import UIKit
import KeychainSwift

class SplashScreenController: UIViewController {
    let keychain = KeychainSwift()
    
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var carImageView: UIImageView!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        view.addGradient(color: UIColor.systemYellow, startPoint: nil, endPoint: nil)
        animate()
    }
    
    func animate() {
        logoImageView.alpha = 0
        carImageView.frame.origin.x -= 400
        carImageView.alpha = 0
        UIView.animate(withDuration: 2, animations: {
            self.logoImageView.alpha = 1
            self.carImageView.frame.origin.x += 400
            self.carImageView.alpha = 1
        }, completion: { finished in
            if(finished) {
                self.doTasks()
            }
        })
    }
    
    func doTasks() {
        guard let email = keychain.get("email"),
              let password = keychain.get("password") else {
           
            if let loginScreenVC = UIStoryboard(name: "LoginScreen", bundle: .main).instantiateInitialViewController() {
                let navigationVC = CustomNavigationController()
                navigationVC.viewControllers = [loginScreenVC]
                navigationVC.modalPresentationStyle = .fullScreen
                self.present(navigationVC, animated: true)
            }
            return
        }
        
        let authManager = AuthManager()
        let announcementManager = AnnouncementManager()
        authManager.login(email: email, password: password).done { success in
            guard success else {
                if let loginScreenVC = UIStoryboard(name: "LoginScreen", bundle: .main).instantiateInitialViewController() {
                    let navigationVC = CustomNavigationController()
                    navigationVC.viewControllers = [loginScreenVC]
                    navigationVC.modalPresentationStyle = .fullScreen
                    self.present(navigationVC, animated: true)
                }
                return
            }
            announcementManager.getUserFavoriteAnnouncements(userId: CurrentUser.shared.userId).done { success in
                for announcement in success {
                    CurrentUser.shared.favoriteAnnouncementsIds.append(announcement.1["announcement_id"].intValue)
                }
            }.catch { error in print(error) }

            if let exploreVC = UIStoryboard(name: "ExploreScreen", bundle: .main).instantiateInitialViewController() {
                let navigationController = CustomNavigationController()
                navigationController.viewControllers = [exploreVC]
                navigationController.modalPresentationStyle = .fullScreen
                self.present(navigationController, animated: true)
            }
        }.catch { error in print(error) }
    }
}
