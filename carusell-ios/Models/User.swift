//
//  User.swift
//  carusell-ios
//
//  Created by Adrian Cimpean on 01.05.2021.
//

import Foundation

public class User {
    public private(set) var userId: Int
    public private(set) var email: String
    public private(set) var firstName: String
    public private(set) var lastName: String
    public private(set) var phoneNumber: String
    public private(set) var cityId: Int
    
    init(userId: Int, email: String, firstName: String, lastName: String, phoneNumber: String, cityId: Int) {
        self.userId = userId
        self.email = email
        self.firstName = firstName
        self.lastName = lastName
        self.phoneNumber = phoneNumber
        self.cityId = cityId
    }
}
