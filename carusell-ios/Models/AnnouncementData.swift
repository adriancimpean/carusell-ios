//
//  AnnouncementData.swift
//  carusell-ios
//
//  Created by Adrian Cimpean on 30.04.2021.
//

import Foundation

public class AnnouncementData {
    public private(set) var title: String
    public private(set) var makeId: Int
    public private(set) var modelId: Int
    public private(set) var year: Int
    public private(set) var bodyTypeId: Int
    public private(set) var kilometers: Int
    public private(set) var fuelTypeId: Int
    public private(set) var transmissionTypeId: Int
    public private(set) var engineSize: Int
    public private(set) var enginePower: Int
    public private(set) var price: Int
    public private(set) var cityId: Int
    public private(set) var description: String
    
    init(title: String, makeId: Int, modelId: Int, year: Int, bodyTypeId: Int, kilometers: Int, fuelTypeId: Int, transmissionTypeId: Int, engineSize: Int, enginePower: Int, price: Int, cityId: Int, description: String) {
        
        self.title = title
        self.makeId = makeId
        self.modelId = modelId
        self.year = year
        self.bodyTypeId = bodyTypeId
        self.kilometers = kilometers
        self.fuelTypeId = fuelTypeId
        self.transmissionTypeId = transmissionTypeId
        self.engineSize = engineSize
        self.enginePower = enginePower
        self.price = price
        self.cityId = cityId
        self.description = description
    }
}
