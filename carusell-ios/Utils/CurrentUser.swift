//
//  CurrentUser.swift
//  carusell-ios
//
//  Created by Adrian Cimpean on 18.04.2021.
//

import Foundation
import SwiftyJSON

public class CurrentUser {
    public private(set) var userId: Int
    public private(set) var token: String
    public private(set) var firstName: String
    public private(set) var lastName: String
    public private(set) var email: String
    public private(set) var phone: String
    public private(set) var cityId: Int
    public var favoriteAnnouncementsIds: [Int]
    
    public static let shared = CurrentUser()
    private init() {
        self.userId = 0
        self.token = ""
        self.firstName = ""
        self.lastName = ""
        self.email = ""
        self.phone = ""
        self.cityId = 0
        self.favoriteAnnouncementsIds = []
    }
    
    public func setCurrentUser(userId: Int, token: String, firstName: String, lastName: String, email: String, phone: String, cityId: Int) {
        CurrentUser.shared.userId = userId
        CurrentUser.shared.token = token
        CurrentUser.shared.firstName = firstName
        CurrentUser.shared.lastName = lastName
        CurrentUser.shared.email = email
        CurrentUser.shared.phone = phone
        CurrentUser.shared.cityId = cityId
    }
}
