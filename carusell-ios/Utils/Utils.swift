//
//  Utils.swift
//  carusell-ios
//
//  Created by Adrian Cimpean on 29.05.2021.
//

import Foundation
import UIKit

public class Utils {
    public static func loadImage(imageUrl: String, imageView: UIImageView) {
        let imageUrl = imageUrl
        guard let url = URL(string: imageUrl) else { return }
        let data = NSData(contentsOf: url)
        let image = UIImage(data: data! as Data)
        imageView.image = image
    }
}
