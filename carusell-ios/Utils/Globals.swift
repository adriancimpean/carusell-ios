//
//  Globals.swift
//  carusell-ios
//
//  Created by Adrian Cimpean on 18.04.2021.
//

import Foundation

public struct Globals {
    static let baseURL = "https://carusell-api.herokuapp.com/api"
    static let imgurClientID = "dfd2536a03105e8"
    
}
