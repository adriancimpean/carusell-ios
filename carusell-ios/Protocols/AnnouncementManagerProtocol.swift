//
//  AnnouncementManager.swift
//  carusell-ios
//
//  Created by Adrian Cimpean on 21.06.2021.
//

import PromiseKit
import SwiftyJSON

protocol AnnouncementManagerProtocol {
    // MARK: - Vehicle
    func getVehicle(id: String) -> Promise<JSON>
    func getData(requestType: RequestType) -> Promise<JSON>
    func getModelsForMakeId(requestType: RequestType) -> Promise<JSON>
   
    // MARK: - Announcement
    func createAnnouncement(announcementData: AnnouncementData, image: UIImage) -> Promise<JSON>
    func editAnnouncement(id: Int, announcementData: AnnouncementData) -> Promise<JSON>
    func deleteAnnouncement(id: Int) -> Promise<JSON>
   
    // MARK: - Favorite Announcements
    func getUserFavoriteAnnouncements(userId: Int) -> Promise<JSON>
    func addAnnouncementToFavorites(announcementId: Int, userId: Int) -> Promise<JSON>
    func removeFromFavorites(favoriteAnnouncementId: Int) -> Promise<JSON>
    
    // MARK: - Announcement Images
    func getAnnouncementImages(announcementId: Int) -> Promise<JSON>
    func saveImageToDatabase(annoucementId: Int, photoUrl: String) -> Promise<JSON>
    func updateAnnouncementImage(announcementId: Int, photoUrl: String) -> Promise<JSON>
    
    // MARK: - Filter
    func filterAnnouncements(filters: [String]) -> Promise<JSON>
    
    // MARK: - User Active Announcements
    func getUserActiveAnnouncements(userId: Int) -> Promise<JSON>
}
