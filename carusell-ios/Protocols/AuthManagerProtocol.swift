//
//  AuthManagerProtocol.swift
//  carusell-ios
//
//  Created by Adrian Cimpean on 21.06.2021.
//

import PromiseKit

protocol AuthManagerProtocol {
    func login(email: String, password: String) -> Promise<Bool>
    func register(email: String, password:String, passwordConfirmation: String, firstName: String, lastName: String, phone: String, cityId: Int) -> Promise<Bool>
}
