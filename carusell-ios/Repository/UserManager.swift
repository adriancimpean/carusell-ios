//
//  UserManager.swift
//  carusell-ios
//
//  Created by Adrian Cimpean on 01.05.2021.
//

import Foundation
import SwiftyJSON
import PromiseKit

public class UserManager {
    func updateUser(user: User, userId: Int) -> Promise<JSON> {
        guard let request = RequestBuilder.request(RequestType.updateUser(user: user, userId: userId)) else { return .rejected }
        return Promise<JSON>.init { resolver in
            URLSession.shared.dataTask(with: request) { data, response, error in
                if let error = error { resolver.reject(error) }
                guard let data = data, let jsonData = try? JSON(data: data) else {
                    resolver.reject(NSError())
                    return
                }
                resolver.fulfill(jsonData)
            }.resume()
        }
    }
}
