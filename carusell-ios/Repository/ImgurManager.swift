//
//  Imgur.swift
//  carusell-ios
//
//  Created by Adrian Cimpean on 23.05.2021.
//

import Foundation
import UIKit
import PromiseKit
import SwiftyJSON

public class ImgurManager {
    func uploadImage(image: UIImage) -> Promise<JSON>? {
        let imgurURL = "https://api.imgur.com/3/image"
        let imageData: NSData = image.pngData()! as NSData
        let base64Image: String = imageData.base64EncodedString(options: .lineLength64Characters)
        
        guard let url = URL(string: imgurURL) else { return nil }
        var request = URLRequest(url: url)
        request.httpMethod = RequestMethod.post.rawValue
        request.addValue("Client-ID dfd2536a03105e8", forHTTPHeaderField: "Authorization")
        
        let boundary = UUID().uuidString
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        let body = NSMutableData()
        body.append("--\(boundary)\r\n".data(using: .utf8)!)
        body.append("Content-Disposition: form-data; name=\"image\"\r\n\r\n".data(using: .utf8)!)
        body.append(base64Image.data(using: .utf8)!)
        body.append("\r\n".data(using: .utf8)!)
        body.append("--\(boundary)--\r\n".data(using: .utf8)!)
        request.httpBody = body as Data
        
        return Promise<JSON>.init { resolver in
            URLSession.shared.dataTask(with: request) { data, response, error in
                if let error = error { resolver.reject(error) }
                guard let data = data, let jsonData = try? JSON(data: data) else {
                    resolver.reject(NSError())
                    return
                }
                resolver.fulfill(jsonData)
            }.resume()
        }
    }
}
