//
//  RequestBuilder.swift
//  carusell-ios
//
//  Created by Adrian Cimpean on 19.04.2021.
//

import Foundation
import PromiseKit
import SwiftyJSON

public enum RequestType {
    //MARK: - AUTH
    case login(email: String, password: String)
    case register(email: String, password: String, passwordConfirmation: String, firstName: String, lastName: String, phone: String, cityId: Int)
    
    // MARK: - ANNOUNCEMENTS
    case getAnnouncements(token: String)
    case postAnnouncement(data: AnnouncementData)
    case deleteAnnouncement(id: Int)
    case editAnnouncement(id: Int, data: AnnouncementData)
    case updateAnnouncementImage(announcementId: Int, photoUrl: String)
    case getUserActiveAnnouncements(userId: Int)
    case getVehicle(id: String)
    case getUserFavoriteAnnouncements(userId: Int)
    case addToFavorites(announcementId: Int ,userId: Int)
    case removeFromFavorites(favoriteAnnouncementId: Int)
    case getAnnouncementImages(announcementId: Int)
    case addAnnouncementImage(announcementId: Int, photoUrl: String)
    case filterAnnouncements(filters: [String])
    
    //MARK: - MODELS
    case getMakes(token: String)
    case getModelsForMakeId(token: String, makeId: Int)
    case getBodyTypes(token: String)
    case getFuelTypes(token: String)
    case getTransmissionTypes(token: String)
    case getCities(token: String)
    
    //MARK: - USER
    case updateUser(user: User, userId: Int)
}

public enum RequestMethod: String {
    case post = "POST"
    case get = "GET"
    case delete = "DELETE"
    case put = "PUT"
}

//MARK: - ENDPOINTS
public class RequestBuilder {
    public static func request(_ requestType: RequestType) -> URLRequest? {
        var urlString = Globals.baseURL
        
        switch requestType {
        //MARK: - AUTH
        case .login:
            urlString.append("/login")
        case .register:
            urlString.append("/register")
            
        //MARK: - ANNOUNCEMENTS
        case .getAnnouncements:
            urlString.append("/announcements")
        case .postAnnouncement:
            urlString.append("/announcements/create")
        case .deleteAnnouncement(let id):
            urlString.append("/announcements/delete/\(id)")
        case .editAnnouncement(let id, _):
            urlString.append("/announcements/update/\(id)")
        case .filterAnnouncements(filters: let filters):
            urlString.append("/announcements/filter?")
            var filterString = ""
            filters.forEach { filter in
                filterString.append(filter)
                filterString.append("&")
            }
            let processedFiltersString: String = String(filterString.dropLast())
            urlString.append(processedFiltersString)
            
        //MARK: -VEHICLES
        case .getVehicle(let id):
            urlString.append("/vehicles/vehicle/\(id)")
        case .getMakes:
            urlString.append("/makes")
        case .getModelsForMakeId( _, let makeId):
            urlString.append("/models/\(makeId)")
        case .getBodyTypes:
            urlString.append("/vehicles/bodyTypes")
        case .getFuelTypes:
            urlString.append("/vehicles/fuelTypes")
        case .getTransmissionTypes:
            urlString.append("/vehicles/transmissions")
            
        //MARK: - FAVORITE ANNOUNCEMENTS
        case .getUserFavoriteAnnouncements(userId: let userId):
            urlString.append("/favorites/\(userId)")
        case .addToFavorites(announcementId: _, userId: _):
            urlString.append("/favorites/addToFavorites")
        case .removeFromFavorites(favoriteAnnouncementId: let favoriteAnnouncementId):
            urlString.append("/favorites/removeFavorite/\(favoriteAnnouncementId)")
       
        //MARK: - ANNOUNCEMENT IMAGES
        case .updateAnnouncementImage(let id, _):
            urlString.append("/announcements/\(id)/updateImage")
        case .getAnnouncementImages(announcementId: let announcementId):
            urlString.append("/announcements/images/\(announcementId)")
        case .addAnnouncementImage:
            urlString.append("/announcements/addImage")
       
        //MARK: - USER
        case .getCities:
            urlString.append("/cities")
        case .updateUser(_, userId: let userId):
            urlString.append("/users/update/\(userId)")
        case .getUserActiveAnnouncements(let userId):
            urlString.append("/announcements/user/\(userId)")
        }
        
        guard let url = URL(string: urlString) else { return nil }
        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")

        switch requestType {
        case .getVehicle, .getMakes, .getModelsForMakeId, .getBodyTypes, .getFuelTypes, .getTransmissionTypes, .getAnnouncements, .getCities, .filterAnnouncements, .getUserActiveAnnouncements, .getUserFavoriteAnnouncements, .getAnnouncementImages:
            request.httpMethod = RequestMethod.get.rawValue
        case .deleteAnnouncement, .removeFromFavorites:
            request.httpMethod = RequestMethod.delete.rawValue
        case .login(email: let email, password: let password):
            request.httpMethod = RequestMethod.post.rawValue
            var userCredentials: [String: String] = [:]
            userCredentials["password"] = password
            userCredentials["email"] = email
            let userJSON = JSON(userCredentials)
            request.httpBody = try? userJSON.rawData()
        case .register(email: let email, password: let password, passwordConfirmation: let passwordConfirmation, firstName: let firstName, lastName: let lastName, phone: let phone, cityId: let cityId):
            request.httpMethod = RequestMethod.post.rawValue
            var userCredentials: [String: Any] = [:]
            userCredentials["email"] = email
            userCredentials["password"] = password
            userCredentials["password_confirmation"] = passwordConfirmation
            userCredentials["first_name"] = firstName
            userCredentials["last_name"] = lastName
            userCredentials["phone"] = phone
            userCredentials["city_id"] = cityId
            let userJSON = JSON(userCredentials)
            request.httpBody = try? userJSON.rawData()
        case .postAnnouncement(data: let announcementData):
            request.httpMethod = RequestMethod.post.rawValue
            var announcementDataDictionary: [String:Any] = [:]
            announcementDataDictionary["make_id"] = announcementData.makeId
            announcementDataDictionary["model_id"] = announcementData.modelId
            announcementDataDictionary["year"] = announcementData.year
            announcementDataDictionary["kilometers"] = announcementData.kilometers
            announcementDataDictionary["bodyType_id"] = announcementData.bodyTypeId
            announcementDataDictionary["engine_size"] = announcementData.engineSize
            announcementDataDictionary["transmissionType_id"] = announcementData.transmissionTypeId
            announcementDataDictionary["price"] = announcementData.price
            announcementDataDictionary["power"] = announcementData.enginePower
            announcementDataDictionary["fuelType_id"] = announcementData.fuelTypeId
            announcementDataDictionary["title"] = announcementData.title
            announcementDataDictionary["description"] = announcementData.description
            announcementDataDictionary["city_id"] = announcementData.cityId
            announcementDataDictionary["user_id"] = CurrentUser.shared.userId
            let announcementJSON = JSON(announcementDataDictionary)
            request.httpBody = try? announcementJSON.rawData()
        case .editAnnouncement(id: _, data: let announcementData):
            request.httpMethod = RequestMethod.put.rawValue
            var announcementDataDictionary: [String: Any] = [:]
            announcementDataDictionary["make_id"] = announcementData.makeId
            announcementDataDictionary["model_id"] = announcementData.modelId
            announcementDataDictionary["year"] = announcementData.year
            announcementDataDictionary["kilometers"] = announcementData.kilometers
            announcementDataDictionary["bodyType_id"] = announcementData.bodyTypeId
            announcementDataDictionary["engine_size"] = announcementData.engineSize
            announcementDataDictionary["transmissionType_id"] = announcementData.transmissionTypeId
            announcementDataDictionary["price"] = announcementData.price
            announcementDataDictionary["power"] = announcementData.enginePower
            announcementDataDictionary["fuelType_id"] = announcementData.fuelTypeId
            announcementDataDictionary["title"] = announcementData.title
            announcementDataDictionary["description"] = announcementData.description
            announcementDataDictionary["city_id"] = announcementData.cityId
            announcementDataDictionary["user_id"] = CurrentUser.shared.userId
            let announcementJSON = JSON(announcementDataDictionary)
            request.httpBody = try? announcementJSON.rawData()
        case .updateAnnouncementImage(announcementId: let announcementId, photoUrl: let photoUrl):
            request.httpMethod = RequestMethod.put.rawValue
            var announcementImageDictionary: [String: Any] = [:]
            announcementImageDictionary["announcement_id"] = announcementId
            announcementImageDictionary["photo_url"] = photoUrl
            let announcementImageJSON = JSON(announcementImageDictionary)
            request.httpBody = try? announcementImageJSON.rawData()
        case .addToFavorites(announcementId: let announcement_id, userId: let user_id):
            request.httpMethod = RequestMethod.post.rawValue
            var favoriteAnnouncementDataDictionary: [String: Any] = [:]
            favoriteAnnouncementDataDictionary["announcement_id"] = announcement_id
            favoriteAnnouncementDataDictionary["user_id"] = user_id
            let favoriteAnnouncementJSON = JSON(favoriteAnnouncementDataDictionary)
            request.httpBody = try? favoriteAnnouncementJSON.rawData()
        case .addAnnouncementImage(announcementId: let announcementId, photoUrl: let photoUrl):
            request.httpMethod = RequestMethod.post.rawValue
            var announcementImageDictionary: [String:Any] = [:]
            announcementImageDictionary["announcement_id"] = announcementId
            announcementImageDictionary["photo_url"] = photoUrl
            let announcementImageJSON = JSON(announcementImageDictionary)
            request.httpBody = try? announcementImageJSON.rawData()
        case .updateUser(user: let user, userId: let userId):
            request.httpMethod = RequestMethod.put.rawValue
            var userDictionary: [String:Any] = [:]
            userDictionary["user_id"] = userId
            userDictionary["email"] = user.email
            userDictionary["first_name"] = user.firstName
            userDictionary["last_name"] = user.lastName
            userDictionary["phone"] = user.phoneNumber
            userDictionary["city_id"] = user.cityId
            let userJSON = JSON(userDictionary)
            request.httpBody = try? userJSON.rawData()
        }
        request.setValue( "Bearer \(CurrentUser.shared.token)", forHTTPHeaderField: "Authorization")
        return request
    }
}
