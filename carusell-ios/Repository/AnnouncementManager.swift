//
//  AnnouncementManager.swift
//  carusell-ios
//
//  Created by Adrian Cimpean on 19.04.2021.
//

import Foundation
import PromiseKit
import SwiftyJSON

public class AnnouncementManager: AnnouncementManagerProtocol {
    
    // MARK: - Vehicle
    func getVehicle(id: String) -> Promise<JSON> {
        guard let request = RequestBuilder.request(RequestType.getVehicle(id: id)) else { return .rejected }
        return returnData(request: request)
    }
    
    func getData(requestType: RequestType) -> Promise<JSON> {
        guard let request = RequestBuilder.request(requestType) else { return .rejected }
        return returnData(request: request)
    }
    
    func getModelsForMakeId(requestType: RequestType) -> Promise<JSON> {
        guard let request = RequestBuilder.request(requestType) else { return .rejected }
        return returnData(request: request)
    }
    
    // MARK: - Announcement
    func createAnnouncement(announcementData: AnnouncementData, image: UIImage) -> Promise<JSON> {
        guard let request = RequestBuilder.request(.postAnnouncement(data: announcementData)) else { return .rejected }
        return returnData(request: request)
    }
    
    func editAnnouncement(id: Int, announcementData: AnnouncementData) -> Promise<JSON> {
        guard let request = RequestBuilder.request(.editAnnouncement(id: id, data: announcementData)) else { return .rejected
        }
        
        return returnData(request: request)
    }
    
    func deleteAnnouncement(id: Int) -> Promise<JSON> {
        guard let request = RequestBuilder.request(.deleteAnnouncement(id: id)) else { return .rejected }
        return returnData(request: request)
    }
    
    // MARK: - Favorites
    
    func getUserFavoriteAnnouncements(userId: Int) -> Promise<JSON> {
        guard let request = RequestBuilder.request(.getUserFavoriteAnnouncements(userId: userId)) else { return .rejected }
        
        return returnData(request: request)
    }
    
    func addAnnouncementToFavorites(announcementId: Int, userId: Int) -> Promise<JSON> {
        guard let request = RequestBuilder.request(.addToFavorites(announcementId: announcementId, userId: userId)) else { return .rejected }
        
        return returnData(request: request)
    }
    
    func removeFromFavorites(favoriteAnnouncementId: Int) -> Promise<JSON> {
        guard let request = RequestBuilder.request(.removeFromFavorites(favoriteAnnouncementId: favoriteAnnouncementId)) else { return .rejected }
        
        return returnData(request: request)
    }
    
    // MARK: - Announcement Images
    func getAnnouncementImages(announcementId: Int) -> Promise<JSON> {
        guard let request = RequestBuilder.request(.getAnnouncementImages(announcementId: announcementId)) else { return .rejected }
        
        return returnData(request: request)
    }
    
    func saveImageToDatabase(annoucementId: Int, photoUrl: String) -> Promise<JSON> {
        guard let request = RequestBuilder.request(.addAnnouncementImage(announcementId: annoucementId, photoUrl: photoUrl)) else { return .rejected }
        
        return returnData(request: request)
    }
    
    func updateAnnouncementImage(announcementId: Int, photoUrl: String) -> Promise<JSON> {
        guard let request = RequestBuilder.request(.updateAnnouncementImage(announcementId: announcementId, photoUrl: photoUrl)) else { return .rejected }
        
        return returnData(request: request)
    }
    
    // MARK: - Filter
    func filterAnnouncements(filters: [String]) -> Promise<JSON> {
        guard let request = RequestBuilder.request(.filterAnnouncements(filters: filters)) else { return .rejected }
        
        return returnData(request: request)
    }
    
    // MARK: - User Active Announcements
    func getUserActiveAnnouncements(userId: Int) -> Promise<JSON> {
        guard let request = RequestBuilder.request(.getUserActiveAnnouncements(userId: userId)) else { return .rejected }
        
        return returnData(request: request)
    }
    
    func returnData(request: URLRequest) -> Promise<JSON> {
        return Promise<JSON>.init { resolver in
            URLSession.shared.dataTask(with: request) { data, response, error in
                if let error = error { resolver.reject(error)
                    print(error)
                }
                guard let data = data, let jsonData = try? JSON(data: data) else {
                    resolver.reject(NSError())
                    return
                }
                resolver.fulfill(jsonData)
            }.resume()
        }
    }
}
