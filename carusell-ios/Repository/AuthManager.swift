//
//  AuthManager.swift
//  carusell-ios
//
//  Created by Adrian Cimpean on 18.04.2021.
//

import Foundation
import PromiseKit
import SwiftyJSON
import KeychainSwift

public class AuthManager: AuthManagerProtocol {
    let keychain = KeychainSwift()
    
    func login(email: String, password: String) -> Promise<Bool> {
        guard let request = RequestBuilder.request(RequestType.login(email: email, password: password)) else {
            return Promise.value(false)
        }
        return Promise<Bool>.init { resolver in
            URLSession.shared.dataTask(with: request) { data, response, error in
                if error != nil { resolver.fulfill(false) }
                
                guard let data = data, let jsonData = try? JSON(data: data) else {
                    resolver.fulfill(false)
                    return
                }
                if jsonData["token"].string != nil  {
                    CurrentUser.shared.setCurrentUser(
                        userId: jsonData["user_id"].int ?? 0,
                        token: jsonData["token"].string ?? "",
                        firstName: jsonData["first_name"].string ?? "",
                        lastName: jsonData["last_name"].string ?? "",
                        email: jsonData["email"].string ?? "",
                        phone: jsonData["phone"].string ?? "",
                        cityId: jsonData["city_id"].int  ?? 1)
                    self.addCredentialsToKeychain(email: CurrentUser.shared.email, password: password)
                    resolver.fulfill(true)
                    return
                }
                resolver.fulfill(false)
            }.resume()
        }
    }
    
    func register(email: String, password:String, passwordConfirmation: String, firstName: String, lastName: String, phone: String, cityId: Int) -> Promise<Bool> {
        guard let request = RequestBuilder.request(RequestType.register(email: email, password: password, passwordConfirmation: passwordConfirmation, firstName: firstName, lastName: lastName, phone: phone, cityId: cityId)) else { return Promise.value(false) }
        return Promise<Bool>.init { resolver in
            URLSession.shared.dataTask(with: request) { data, response, error in
                if let error = error { resolver.reject(error) }
                guard let data = data, let jsonData = try? JSON(data: data) else {
                    resolver.reject(NSError())
                    return
                }
                resolver.fulfill(jsonData["code"].intValue == 201)
            }.resume()
        }
    }
    
    func addCredentialsToKeychain(email: String, password: String) {
        keychain.set(email, forKey: "email")
        keychain.set(password, forKey: "password")
    }
}
